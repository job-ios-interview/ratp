//
//  CacheTests.swift
//  RatpTests
//
//  Created by Stephane Mouawad on 14/03/2024.
//

import XCTest
@testable import Ratp

final class CacheTests: XCTestCase {
    func testStore() {
        let cache = Cache(validity: 1)
        
        let dataToSave = "Jambon - Beur".data(using: .utf8)
        cache.store(data: dataToSave, key: #function)
        
        let data = UserDefaults.standard.object(forKey: #function) as! Data
        let content = try! JSONDecoder().decode(CachingContent<Data>.self, from: data)
        XCTAssertNotNil(dataToSave)
        XCTAssertEqual(content.data, dataToSave)
    }
    
    func testFetchSuccess() {
        let cache = Cache(validity: 1)

        let dataToSave = "Jambon - Margarine".data(using: .utf8)
        cache.store(data: dataToSave, key: #function)
        
        let dataTofetch: Data? = cache.fetch(key: #function)
        XCTAssertNotNil(dataToSave)
        XCTAssertEqual(dataTofetch, dataToSave)
    }
    
    func testFetchFailureForInvalidity() {
        let cache = Cache(validity: -1)

        let dataToSave = "Baguette et du demi-sel ehh ho, c'est la bretagne ici ...".data(using: .utf8)
        cache.store(data: dataToSave, key: #function)

        let dataTofetch: Data? = cache.fetch(key: #function)
        XCTAssertNotNil(dataToSave)
        XCTAssertNil(dataTofetch)
    }
    
    func testFetchFailureDataNotFound() {
        let cache = Cache(validity: -1)
        let dataTofetch: Data? = cache.fetch(key: UUID().uuidString)
        XCTAssertNil(dataTofetch)
    }
}
