//
//  RequestBuilderTests.swift
//  RatpTests
//
//  Created by Stephane Mouawad on 14/03/2024.
//

import XCTest
@testable import Ratp

final class RequestBuilderTests: XCTestCase {
    func testBuilder() {
        let url = EndPoint.root
        let request = RequestBuilder.request(for: url)

        XCTAssertEqual(request.url, url)
        XCTAssertEqual(request.httpMethod, "GET")
        XCTAssertEqual(request.allHTTPHeaderFields, ["Content-Type": "application/json"])
    }
}
