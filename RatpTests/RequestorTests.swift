//
//  RequestorTests.swift
//  RatpTests
//
//  Created by Stephane Mouawad on 14/03/2024.
//

import XCTest
@testable import Ratp

private enum Expectation {
    static var cachedData = "{ \"test\" : \"I am to old bro\" }".data(using: .utf8)!
    static var freshData = " { \"test\" : \"Oh yeah baby\" }".data(using: .utf8)!
}

struct MockSession: URLSessionBehaviors, Equatable {
    var configuration: URLSessionConfiguration
    
    func data(for request: URLRequest, delegate: (any URLSessionTaskDelegate)?) async throws -> (Data, URLResponse) {
        return (Expectation.freshData, URLResponse())
    }
}

class RequestorTests: XCTestCase {
    let url = EndPoint.root
    lazy var request = RequestBuilder.request(for: url)

    func testGetFreshData() async {
        let requestorWithInvalidCache = Requestor(session: MockSession(configuration: .default), cache: Cache(validity: -1))
        let request = RequestBuilder.request(for: EndPoint.root)
        let object: [String: String] = try! await requestorWithInvalidCache.fire(with: request)
        XCTAssertEqual(object["test"], "Oh yeah baby")
    }
    
    func testGetCachingData() async {
        Cache(validity: 1).store(data: Expectation.cachedData, key: request.description)
        
        let requestorWithValidCache = Requestor(session: MockSession(configuration: .default), cache: Cache(validity: 1))
        let request = RequestBuilder.request(for: EndPoint.root)
        let object: [String: String] = try! await requestorWithValidCache.fire(with: request)
        XCTAssertEqual(object["test"], "I am to old bro")
    }
    
    func testParsingErrorWithFreshData() async {
        let requestorWithInvalidCache = Requestor(session: MockSession(configuration: .default), cache: Cache(validity: -1))
        let request = RequestBuilder.request(for: EndPoint.root)
        
        do {
            let _: String = try await requestorWithInvalidCache.fire(with: request)
            XCTAssertTrue(false)
        } catch {
            XCTAssertTrue(true)
        }
    }

    func testParsingErrorWithCachingData() async {
        Cache(validity: 1).store(data: Expectation.cachedData, key: request.description)
        
        let requestorWithValidCache = Requestor(session: MockSession(configuration: .default), cache: Cache(validity: 1))
        let request = RequestBuilder.request(for: EndPoint.root)
                
        do {
            let _: String = try await requestorWithValidCache.fire(with: request)
            XCTAssertTrue(false)
        } catch {
            XCTAssertTrue(true)
        }
    }
    
    func testCancelRequest() {
        // Add test case for canceling request
    }
}
