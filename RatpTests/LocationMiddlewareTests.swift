//
//  LocationMiddlewareTests.swift
//  RatpTests
//
//  Created by Stephane Mouawad on 15/03/2024.
//

import Foundation
import CoreLocation

import XCTest
@testable import Ratp

class MockCLLocationManager: CLLocationManager {
    var mockAuthorizationStatus: CLAuthorizationStatus
    var requestWhenInUseAuthorizationHasBeenCalled = false
    var stopUpdatingLocationHasBeenCalled = false
    var startUpdatingLocationHasBeenCalled = false

    var mockLocation: CLLocation?

    override var location: CLLocation? {
        mockLocation
    }

    override var authorizationStatus: CLAuthorizationStatus {
        mockAuthorizationStatus
    }
    
    init(authorizationStatus: CLAuthorizationStatus) {
        mockAuthorizationStatus = authorizationStatus
    }
    
    override func requestWhenInUseAuthorization() {
        requestWhenInUseAuthorizationHasBeenCalled = true
    }
    
    override func stopUpdatingLocation() {
        stopUpdatingLocationHasBeenCalled = true
    }

    override func startUpdatingLocation() {
        startUpdatingLocationHasBeenCalled = true
    }
}

class LocationMiddlewareTests: XCTestCase {
    func testInit() {
        let locationManager = CLLocationManager()
        let middleware = LocationMiddleware(locationManager: locationManager)
        
        XCTAssertNotNil(locationManager.delegate)
        XCTAssertEqual(locationManager.delegate?.hash, middleware.hash)
        XCTAssertEqual(middleware.locationManager, locationManager)
        XCTAssertEqual(locationManager.desiredAccuracy, kCLLocationAccuracyBest)
        XCTAssertNil(middleware.currentLocation)
    }
    
    func testStartUpdatingLocation() {
        var locationManager = MockCLLocationManager(authorizationStatus: .denied)
        var middleware = LocationMiddleware(locationManager: locationManager)
        middleware.startUpdatingLocation()
        XCTAssertTrue(locationManager.requestWhenInUseAuthorizationHasBeenCalled)
        
        locationManager = MockCLLocationManager(authorizationStatus: .restricted)
        middleware = LocationMiddleware(locationManager: locationManager)
        middleware.startUpdatingLocation()
        XCTAssertTrue(locationManager.requestWhenInUseAuthorizationHasBeenCalled)
        
        locationManager = MockCLLocationManager(authorizationStatus: .notDetermined)
        middleware = LocationMiddleware(locationManager: locationManager)
        middleware.startUpdatingLocation()
        XCTAssertTrue(locationManager.requestWhenInUseAuthorizationHasBeenCalled)
        
        locationManager = MockCLLocationManager(authorizationStatus: .authorizedAlways)
        middleware = LocationMiddleware(locationManager: locationManager)
        middleware.startUpdatingLocation()
        XCTAssertFalse(locationManager.requestWhenInUseAuthorizationHasBeenCalled)
        
        locationManager = MockCLLocationManager(authorizationStatus: .authorizedWhenInUse)
        middleware = LocationMiddleware(locationManager: locationManager)
        middleware.startUpdatingLocation()
        XCTAssertFalse(locationManager.requestWhenInUseAuthorizationHasBeenCalled)
    }
    
    func testStopUpdatingLocation() {
        let locationManager = MockCLLocationManager(authorizationStatus: .authorizedWhenInUse)
        let middleware = LocationMiddleware(locationManager: locationManager)
        middleware.stopUpdatingLocation()
        XCTAssertTrue(locationManager.stopUpdatingLocationHasBeenCalled)
    }
    
    func testDidUpdateLocations() {
        let locationManager = MockCLLocationManager(authorizationStatus: .authorizedWhenInUse)
        let middleware = LocationMiddleware(locationManager: locationManager)
        XCTAssertNil(middleware.currentLocation)
        
        locationManager.mockLocation = .init(latitude: 122, longitude: 567)
        middleware.locationManager(locationManager, didUpdateLocations: [])
        XCTAssertEqual(middleware.currentLocation?.latitude, 122)
        XCTAssertEqual(middleware.currentLocation?.longitude, 567)
    }
    
    func testComputeDistanceCurrentLocation() {
        let locationManager = MockCLLocationManager(authorizationStatus: .authorizedWhenInUse)
        let middleware = LocationMiddleware(locationManager: locationManager)
        let result1 = middleware.computeDistanceCurrentLocation(toEndLocation: .init(latitude: 1, longitude: 2))
        XCTAssertNil(result1)
        
        locationManager.mockLocation = .init(latitude: 122, longitude: 567)
        middleware.locationManager(locationManager, didUpdateLocations: [])
        let result2 = middleware.computeDistanceCurrentLocation(toEndLocation: .init(latitude: 1, longitude: 2))
        XCTAssertEqual(result2, 0)
        
    }
    
    func testLocationManagerDidChangeAuthorization() {
        var locationManager = MockCLLocationManager(authorizationStatus: .notDetermined)
        
        var middleware = LocationMiddleware(locationManager: locationManager)
        locationManager.mockAuthorizationStatus = .denied
        middleware.locationManagerDidChangeAuthorization(locationManager)
        XCTAssertFalse(locationManager.startUpdatingLocationHasBeenCalled)
        
        locationManager = MockCLLocationManager(authorizationStatus: .notDetermined)
        middleware = LocationMiddleware(locationManager: locationManager)
        locationManager.mockAuthorizationStatus = .restricted
        middleware.locationManagerDidChangeAuthorization(locationManager)
        XCTAssertFalse(locationManager.startUpdatingLocationHasBeenCalled)
        
        locationManager = MockCLLocationManager(authorizationStatus: .notDetermined)
        middleware = LocationMiddleware(locationManager: locationManager)
        middleware.locationManagerDidChangeAuthorization(locationManager)
        XCTAssertFalse(locationManager.startUpdatingLocationHasBeenCalled)
        
        locationManager = MockCLLocationManager(authorizationStatus: .notDetermined)
        middleware = LocationMiddleware(locationManager: locationManager)
        locationManager.mockAuthorizationStatus = .authorizedAlways
        middleware.locationManagerDidChangeAuthorization(locationManager)
        XCTAssertTrue(locationManager.startUpdatingLocationHasBeenCalled)
        
        locationManager = MockCLLocationManager(authorizationStatus: .notDetermined)
        middleware = LocationMiddleware(locationManager: locationManager)
        locationManager.mockAuthorizationStatus = .authorizedWhenInUse
        middleware.locationManagerDidChangeAuthorization(locationManager)
        XCTAssertTrue(locationManager.startUpdatingLocationHasBeenCalled)
    }
}
