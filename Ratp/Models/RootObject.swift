//
//  RootObject.swift
//  Ratp
//
//  Created by Stephane Mouawad on 13/03/2024.
//

import Foundation

// MARK: - Welcome
struct RootObject: Codable {
    // The main entry point
    let records: [Record]
}
