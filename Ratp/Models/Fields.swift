//
//  File.swift
//  Ratp
//
//  Created by Stephane Mouawad on 13/03/2024.
//

import Foundation

// MARK: - Fields
struct Fields: Codable {
    /// The type of equipment.
    enum FieldsType: String, Codable {
        case lavatory = "LAVATORY"
        case sanisette = "SANISETTE"
        case toilets = "TOILETTES"
        case urinal = "URINOIR"
        case urinalFemale = "URINOIR FEMME"
        case permanentPublicToilets = "WC PUBLICS PERMANENTS"
    }
    
    /// The availability schedule.
    let availability: Availability?
    
    /// Indicates whether there is reduced mobility access.
    let reducedMobilityAccess: Bool
    
    /// Indicates whether there are baby changing facilities.
    let babyChangingFacilities: Bool
    
    /// The district of the location.
    let district: Int
    
    /// The geographical coordinates.
    let geoPoint2D: [Double]
    
    /// The address of the location.
    let address: String?
    
    /// The type of equipment.
    let type: FieldsType
    
    /// The URL for equipment details.
    let equipementURL: String?
    
    /// Coding keys for decoding and encoding purposes.
    enum CodingKeys: String, CodingKey {
        case availability = "horaire"
        case reducedMobilityAccess = "acces_pmr"
        case district = "arrondissement"
        case geoPoint2D = "geo_point_2d"
        case address = "adresse"
        case type
        case babyChangingFacilities = "relais_bebe"
        case equipementURL = "url_fiche_equipement"
    }
    
    /// Custom initializer for decoding.
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.availability = try container.decodeIfPresent(Availability.self, forKey: .availability)
        self.reducedMobilityAccess = try container.decodeIfPresent(String.self, forKey: .reducedMobilityAccess) == "Oui"
        self.district = try container.decode(Int.self, forKey: .district)
        self.geoPoint2D = try container.decode([Double].self, forKey: .geoPoint2D)
        self.address = try container.decodeIfPresent(String.self, forKey: .address)
        self.type = try container.decode(FieldsType.self, forKey: .type)
        self.babyChangingFacilities = try container.decodeIfPresent(String.self, forKey: .babyChangingFacilities) == "Oui"
        self.equipementURL = try container.decodeIfPresent(String.self, forKey: .equipementURL)
    }
}
