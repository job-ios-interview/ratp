//
//  Location.swift
//  Ratp
//
//  Created by Stephane Mouawad on 13/03/2024.
//

import Foundation

// MARK: - Location
struct Location: Codable {
    /// The geographical coordinates.
    let coordinates: [Double]
}
