//
//  Availability.swift
//  Ratp
//
//  Created by Stephane Mouawad on 13/03/2024.
//

import Foundation

enum Availability: String, Codable {
    /// Availability from 10:00 to 18:00.
    case from10AMto6PM = "10h/18h"
    
    /// Availability from 10:00 to 22:00.
    case from10AMto10PM = "10h à 22h"
    
    /// Availability from 19:00 to 07:00.
    case from7PMto7AM = "19 h - 7 h"
    
    /// Available 24 hours a day.
    case twentyFourHours = "24 h / 24"
    
    /// Availability from 6:00 to 01:00.
    case from6AMto1AM = "6 h - 1 h"
    
    /// Availability from 6:00 to 22:00.
    case from6AMto10PM = "6 h - 22 h"
    
    /// Other availability, refer to equipment details.
    case other = "Voir fiche équipement"

    /// Coding keys for decoding and encoding purposes.
    enum CodingKeys: String, CodingKey {
        case from10AMto6PM = "the10H18H"
        case from10AMto10PM = "the10HÀ22H"
        case from7PMto7AM = "the19H7H"
        case twentyFourHours = "the24H24"
        case from6AMto1AM = "the6H1H"
        case from6AMto10PM = "the6H22H"
        case other = "voirFicheÉquipement"
    }
    
    var description: String {
        switch self {
        case .from10AMto6PM:
            "Disponible de 10h à 18h"
        case .from10AMto10PM:
            "Disponible de 10h à 22h"
        case .from7PMto7AM:
            "Disponible de 19h à 7h du matin"
        case .twentyFourHours:
            "Disponible toute la journée"
        case .from6AMto1AM:
            "Disponible de 6h à 1h du matin"
        case .from6AMto10PM:
            "Disponible de 6h à 22h"
        case .other:
            "La fréro, je sais pas ..., Ecosia est ton ami"
        }
    }
}
