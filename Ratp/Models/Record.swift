//
//  Record.swift
//  Ratp
//
//  Created by Stephane Mouawad on 13/03/2024.
//

import Foundation

// MARK: - Record
struct Record: Codable {
    /// The unique identifier of the record.
    let id: String
    
    /// The fields containing the record data.
    let fields: Fields
    
    /// The location information.
    let location: Location
    
    /// The timestamp when the record was created.
    let recordTimestamp: String
    
    /// Coding keys for decoding and encoding purposes.
    enum CodingKeys: String, CodingKey {
        case id = "recordid"
        case fields
        case location = "geometry"
        case recordTimestamp = "record_timestamp"
    }
}
