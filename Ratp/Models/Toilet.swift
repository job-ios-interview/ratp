//
//  Toilet.swift
//  Ratp
//
//  Created by Stephane Mouawad on 14/03/2024.
//

import Foundation
import MapKit

/// Structure representing a toilet.
struct Toilet: Hashable {
    /// The title of the toilet.
    let title: String
    
    /// An indicator indicating whether the toilet is accessible for people with reduced mobility.
    let reducedMobilityAccess: Bool
    
    /// An indicator indicating whether the toilet has baby changing facilities.
    let babyChangingFacilities: Bool
    
    /// The location of the toilet.
    let location: CLLocationCoordinate2D
    
    /// The full street address of the toilet.
    let fullStreetAddress: String
    
    /// The district where the toilet is located.
    let district: Int
    
    /// The availability of the toilet.
    let availability: String
    
    /// Initializes a toilet with the provided fields.
    ///
    /// - Parameter fields: The fields required to initialize a toilet.
    init?(fields: Fields) {
        guard
            let address = fields.address,
            let availability = fields.availability,
            fields.geoPoint2D.count == 2
        else { return nil }
        
        self.title = fields.type.rawValue.capitalized
        self.reducedMobilityAccess = fields.reducedMobilityAccess
        self.babyChangingFacilities = fields.babyChangingFacilities
        self.location = .init(latitude: fields.geoPoint2D[0], longitude: fields.geoPoint2D[1])
        self.fullStreetAddress = address + "\(fields.district)"
        self.district = fields.district
        self.availability = availability.description
    }
    
    /// Generates the hash value of the toilet.
    func hash(into hasher: inout Hasher) {
        hasher.combine(title)
        hasher.combine(reducedMobilityAccess)
        hasher.combine(babyChangingFacilities)
        hasher.combine(location.latitude)
        hasher.combine(location.longitude)
        hasher.combine(fullStreetAddress)
        hasher.combine(district)
        hasher.combine(availability)
    }
    
    /// Checks the equality between two toilets.
    static func == (lhs: Toilet, rhs: Toilet) -> Bool {
        return lhs.title == rhs.title &&
            lhs.reducedMobilityAccess == rhs.reducedMobilityAccess &&
            lhs.babyChangingFacilities == rhs.babyChangingFacilities &&
            lhs.location.latitude == rhs.location.latitude &&
            lhs.location.longitude == rhs.location.longitude &&
            lhs.fullStreetAddress == rhs.fullStreetAddress &&
            lhs.district == rhs.district &&
            lhs.availability == rhs.availability
    }
}
