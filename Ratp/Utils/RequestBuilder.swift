//
//  RequestBuilder.swift
//  Ratp
//
//  Created by Stephane Mouawad on 13/03/2024.
//

import Foundation

// MARK: - EndPoint
public enum EndPoint {
    /// Root URL for the API endpoint.
    static let root = URL(string: "https://data.ratp.fr/api/records/1.0/search/?dataset=sanisettesparis2011&start=0&rows=1000")!
}

// MARK: - RequestBuilder
public struct RequestBuilder {
    /// Builds a URLRequest for the specified URL.
    ///
    /// - Parameters:
    ///   - url: The URL for the request.
    ///   - httpHeader: Additional HTTP headers.
    /// - Returns: The URLRequest instance.
    public static func request(for url: URL, httpHeader: [String: String] = [:]) -> URLRequest {
        var request = URLRequest(url: url)
        request.httpMethod = "GET"
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        httpHeader.forEach { request.setValue($0.value, forHTTPHeaderField: $0.key) }
        return request
    }
}
