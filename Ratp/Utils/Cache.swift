//
//  Cache.swift
//  Ratp
//
//  Created by Stephane Mouawad on 13/03/2024.
//

import Foundation

// MARK: - CachingContent
/// A generic struct used for caching content with a timestamp.
struct CachingContent<T: Codable>: Codable {
    /// The date when the content was cached.
    var recordDate = Date()

    /// The cached data.
    let data: T
}

// MARK: - Cache
struct Cache {
    let validity: TimeInterval

    /// Stores data in the cache with the specified key.
    ///
    /// - Parameters:
    ///   - data: The data to be cached.
    ///   - key: The key to identify the cached data.
    func store<T: Codable>(data: T, key: String) {
        let content = CachingContent(data: data)
        let data = try? JSONEncoder().encode(content)
        UserDefaults.standard.setValue(data, forKey: key)
    }

    /// Retrieves data from the cache for the specified key.
    ///
    /// - Parameter key: The key to identify the cached data.
    /// - Returns: The cached data if available and valid; otherwise, nil.
    func fetch<T: Codable>(key: String) -> T? {
        guard
            let data = UserDefaults.standard.object(forKey: key) as? Data,
            let content = try? JSONDecoder().decode(CachingContent<T>.self, from: data)
        else {
            Log.print(state: .info, message: "[Cache] [Error] Data not found")
            return nil
        }

        guard content.recordDate.addingTimeInterval(validity) > Date.now else {
            UserDefaults.standard.removeObject(forKey: key)
            Log.print(state: .info, message: "[Cache] [Error] Data is expired")
            return nil
        }

        return content.data
    }
}
