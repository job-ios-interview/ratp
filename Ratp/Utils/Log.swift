//
//  Log.swift
//  Ratp
//
//  Created by Stephane Mouawad on 13/03/2024.
//

import Foundation

// MARK: - Log
public struct Log {
    /// Enumeration defining log states.
    public enum State: String {
        case info = "📣📣📣 - Info"
        case error = "🔥🔥🔥 - Error"
    }

    /// Prints log messages.
    ///
    /// - Parameters:
    ///   - state: The state of the log message.
    ///   - message: The message to be logged.
    ///   - object: The optional object associated with the log.
    ///   - file: The file where the log statement originated.
    ///   - line: The line number where the log statement originated.
    ///   - function: The function where the log statement originated.
    public static func print(
        state: Log.State,
        message: @autoclosure () -> String,
        object: AnyObject? = nil,
        file: String = #file,
        line: Int = #line,
        function: String = #function) {
 
#if DEBUG
        let address = object.map { Unmanaged.passUnretained($0).toOpaque() }
        let lastPathComponent = URL(fileURLWithPath: file).lastPathComponent
        let message = message().isEmpty ? "" : "-" + message()
        Swift.print(state.rawValue, "-", line, "-", lastPathComponent, "-", address ?? "Unknown", "-", function, message)
#endif
    }
}
