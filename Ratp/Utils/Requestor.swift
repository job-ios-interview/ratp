//
//  Requestor.swift
//  Ratp
//
//  Created by Stephane Mouawad on 13/03/2024.
//

import Foundation

protocol URLSessionBehaviors {
    var configuration: URLSessionConfiguration { get }
    func data(for request: URLRequest, delegate: (any URLSessionTaskDelegate)?) async throws -> (Data, URLResponse)
}

extension URLSession: URLSessionBehaviors {}

// MARK: - Requestor
class Requestor {
    let session: URLSessionBehaviors
    let cache: Cache

    /// Initializes a new instance of Requestor.
    ///
    /// - Parameters:
    ///   - session: The URLSession to use for network requests.
    ///   - cache: The cache to store and retrieve data, The default validity interval for cached data (default is 1 hour).
    init(session: URLSessionBehaviors = URLSession.shared, cache: Cache) {
        self.session = session
        self.cache = cache
        self.session.configuration.requestCachePolicy = .reloadRevalidatingCacheData
    }

    /// Executes a network request and decodes the response into a Codable object.
    ///
    /// - Parameters:
    ///   - request: The URLRequest object for the request.
    /// - Returns: A Codable object decoded from the response data.
    func fire<T: Codable>(with request: URLRequest) async throws -> T {
        do {
            Log.print(state: .info, message: "Start: - " + request.description)
            
            if let data: Data = cache.fetch(key: request.description) {
                Log.print(state: .info, message: "Cache is reused: - " + request.description)
                
                let object = try JSONDecoder().decode(T.self, from: data)
                return object
            }

            let data = try await session.data(for: request, delegate: nil)
            let object = try JSONDecoder().decode(T.self, from: data.0)
            
            Log.print(state: .info, message: "Success: - " + request.description)
            cache.store(data: data.0, key: request.description)
            
            return object
        } catch {
            Log.print(state: .error, message: "Failure: - " + request.description)
            throw error
        }
    }
    
    /// Cancels an ongoing operation.
    ///
    /// This method is used to cancel an ongoing operation.
    /// It should be implemented in the future.
    /// This method is mandatory for this exercise.
    func cancel() {
        // Should be implemented in the future.
        // This is mandatory for this exercise.
    }
}
