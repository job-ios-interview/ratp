//
//  SearchBarView.swift
//  Ratp
//
//  Created by Stephane Mouawad on 14/03/2024.
//

import UIKit

// MARK: - SearchBarViewViewModel

/// View model for the search bar view.
class SearchBarViewViewModel {
    /// Foreground color for the search bar.
    let forground = UIColor(named: "SearchBar.forground")
    
    /// Image for the location icon.
    let locationImage = UIImage(named: "location")
    
    /// Text color for the search bar text field.
    let textColor = UIColor(named: "SearchBar.text")
    
    /// Placeholder text for the search bar.
    let placeHolder = NSAttributedString(
        string: "Votre recherche",
        attributes: [
            NSAttributedString.Key.foregroundColor: UIColor(named: "SearchBar.placeholder") as Any
        ]
    )

    /// Closure to be called when the text in the search bar changes.
    var onChange: ((String) -> Void)?
    
    /// Action method called when the text field content changes.
    @objc
    func textFieldDidChange(textField: UITextField) {
        onChange?(textField.text ?? "")
    }
}

// MARK: - SearchBarView

/// Custom search bar view.
class SearchBarView: UIView {
    var viewModel: SearchBarViewViewModel
    
    /// Image view for the location icon.
    let imageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()

    /// Text field for entering search queries.
    let textField: UITextField = UITextField()
    
    /// Initializes the search bar view with a view model.
    init(viewModel: SearchBarViewViewModel) {
        self.viewModel = viewModel
        super.init(frame: .zero)
        setupFront()
        setupLayout()
    }

    /// Initializes the search bar view from storyboard or xib.
    required init?(coder: NSCoder) {
        self.viewModel = SearchBarViewViewModel()
        super.init(coder: coder)
        setupFront()
        setupLayout()
    }

    /// Sets up the appearance and behavior of the search bar view.
    private func setupFront() {
        addSubview(imageView)
        addSubview(textField)
        backgroundColor = viewModel.forground
        textField.addTarget(viewModel, action: #selector(viewModel.textFieldDidChange), for: .editingChanged)
        textField.textColor = viewModel.textColor
        textField.attributedPlaceholder = viewModel.placeHolder
        imageView.image = viewModel.locationImage
    }

    /// Sets up the layout constraints for subviews.
    private func setupLayout() {
        imageView.translatesAutoresizingMaskIntoConstraints = false
        textField.translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint.activate([
            // Setup constraints for imageView
            imageView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 20),
            imageView.topAnchor.constraint(equalTo: topAnchor, constant: 8),
            imageView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -8),
            imageView.widthAnchor.constraint(equalTo: imageView.heightAnchor),
            
            // Setup constraints for textField
            textField.leadingAnchor.constraint(equalTo: imageView.trailingAnchor, constant: 8),
            textField.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -8),
            textField.topAnchor.constraint(equalTo: topAnchor),
            textField.bottomAnchor.constraint(equalTo: bottomAnchor)
        ])
    }
}
