//
//  ToiletCardCell.swift
//  Ratp
//
//  Created by Stephane Mouawad on 15/03/2024.
//

import UIKit
import CoreLocation

struct ToiletCardCellViewModel {
    let title: String
    let subtitle: String
    let distance: String
    let availability: String
    var accessibilityImages: [UIImage] = []
    
    init(toilet: Toilet, middleware: LocationMiddleware) {
        self.title = toilet.title
        self.subtitle = toilet.fullStreetAddress
        self.availability = toilet.availability
        
        if let distance = middleware.computeDistanceCurrentLocation(toEndLocation: toilet.location)?.rounded() {
            self.distance = "Distance: \(distance / 100)km"
        } else {
            self.distance = ""
        }
        
        if toilet.babyChangingFacilities {
            accessibilityImages.append(UIImage(systemName: "figure.and.child.holdinghands") ?? UIImage())
        }
        
        if toilet.reducedMobilityAccess {
            accessibilityImages.append(UIImage(systemName: "figure.roll") ?? UIImage())
        }
    }
}

class ToiletCardCell: UICollectionViewCell {
    let roundedBackgroundView = UIView()
    let titleLabel = UILabel()
    let subtitleLabel = UILabel()
    let distanceLabel = UILabel()
    let availabilityLabel = UILabel()
    let hstack = UIStackView()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupFront()
        setupLayout()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setupFront()
        setupLayout()
    }
    
    private func setupFront() {
        hstack.axis = .vertical
        hstack.alignment = .top
        hstack.distribution = .fill
        
        roundedBackgroundView.backgroundColor = UIColor(named: "Card.forground")
        roundedBackgroundView.layer.cornerRadius = 12
        
        titleLabel.textColor = UIColor(named: "Card.text")
        titleLabel.font = .preferredFont(forTextStyle: .title1)
        
        subtitleLabel.textColor = UIColor(named: "Card.text")
        subtitleLabel.font = .preferredFont(forTextStyle: .title2)
        subtitleLabel.numberOfLines = 3
        
        distanceLabel.textColor = UIColor(named: "Card.text")
        distanceLabel.font = .preferredFont(forTextStyle: .title2)
        
        availabilityLabel.textColor = UIColor(named: "Card.text")
        availabilityLabel.numberOfLines = 3
        availabilityLabel.font = .preferredFont(forTextStyle: .title2)
        
        [roundedBackgroundView, availabilityLabel, titleLabel, subtitleLabel, distanceLabel, hstack].forEach {
            addSubview($0)
        }
    }
    
    private func setupLayout() {
        [roundedBackgroundView, availabilityLabel, titleLabel, subtitleLabel, distanceLabel, hstack].forEach {
            $0.translatesAutoresizingMaskIntoConstraints = false
        }
        
        NSLayoutConstraint.activate([
            // Setup constraints for titleLabel
            roundedBackgroundView.leadingAnchor.constraint(equalTo: leadingAnchor),
            roundedBackgroundView.trailingAnchor.constraint(equalTo: trailingAnchor),
            roundedBackgroundView.topAnchor.constraint(equalTo: topAnchor),
            roundedBackgroundView.bottomAnchor.constraint(equalTo: bottomAnchor),
            
            // Setup constraints for titleLabel
            titleLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 10),
            titleLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -10),
            titleLabel.topAnchor.constraint(equalTo: topAnchor, constant: 10),
            
            // Setup constraints for searchBarView
            availabilityLabel.leadingAnchor.constraint(equalTo: titleLabel.leadingAnchor),
            availabilityLabel.trailingAnchor.constraint(equalTo: titleLabel.trailingAnchor),
            availabilityLabel.bottomAnchor.constraint(equalTo: subtitleLabel.topAnchor, constant: -10),
            
            // Setup constraints for searchBarView
            subtitleLabel.leadingAnchor.constraint(equalTo: titleLabel.leadingAnchor),
            subtitleLabel.trailingAnchor.constraint(equalTo: titleLabel.trailingAnchor),
            subtitleLabel.bottomAnchor.constraint(equalTo: distanceLabel.topAnchor, constant: -10),
            
            distanceLabel.leadingAnchor.constraint(equalTo: titleLabel.leadingAnchor),
            distanceLabel.trailingAnchor.constraint(equalTo: titleLabel.trailingAnchor),
            distanceLabel.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -20),
            
            // Setup constraints for searchBarView
            hstack.widthAnchor.constraint(equalToConstant: 30),
            hstack.heightAnchor.constraint(equalToConstant: 100),
            hstack.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -10),
            hstack.topAnchor.constraint(equalTo: topAnchor),
        ])
    }

    func update(withViewModel viewModel: ToiletCardCellViewModel) {
        titleLabel.text = viewModel.title
        subtitleLabel.text = viewModel.subtitle
        distanceLabel.text = viewModel.distance
        availabilityLabel.text = viewModel.availability

        hstack.arrangedSubviews.forEach {
            $0.removeFromSuperview()
        }

        viewModel.accessibilityImages.forEach {
            let image = UIImageView()
            image.image = $0
            image.contentMode = .scaleAspectFit
            hstack.addArrangedSubview(image)
        }
    }
}
