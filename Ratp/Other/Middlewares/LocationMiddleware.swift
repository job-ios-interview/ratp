//
//  LocationMiddleware.swift
//  Ratp
//
//  Created by Stephane Mouawad on 15/03/2024.
//

import Foundation
import CoreLocation

/// A middleware class responsible for managing location services and computing distances.
class LocationMiddleware: NSObject, CLLocationManagerDelegate {
    /// The location manager responsible for handling location updates.
    let locationManager: CLLocationManager
    /// The current user location.
    var currentLocation: CLLocationCoordinate2D?
    
    /// Initializes a `LocationMiddleware` instance with a given `CLLocationManager`.
    ///
    /// - Parameter locationManager: The `CLLocationManager` instance to be used.
    init(locationManager: CLLocationManager) {
        self.locationManager = locationManager
        super.init()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
    }

    /// Starts updating the user's location.
    func startUpdatingLocation() {
        if [.restricted, .denied, .notDetermined].contains(locationManager.authorizationStatus) {
            locationManager.requestWhenInUseAuthorization()
        }
    }
    
    /// Stops updating the user's location.
    func stopUpdatingLocation() {
        locationManager.stopUpdatingLocation()
    }
    
    /// Computes the distance from the current location to a given destination.
    ///
    /// - Parameter location: The destination location.
    /// - Returns: The computed distance in meters.
    func computeDistanceCurrentLocation(toEndLocation location: CLLocationCoordinate2D) -> Double? {
        let end = CLLocation(latitude: location.latitude, longitude: location.longitude)
        let stat = currentLocation.map { CLLocation(latitude: $0.latitude, longitude: $0.longitude) }
        return stat?.distance(from: end)
    }
    
    /// Delegate method called when the location manager receives updated locations.
    ///
    /// - Parameters:
    ///   - manager: The location manager object.
    ///   - locations: An array of CLLocation objects containing the location data.
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        currentLocation = manager.location?.coordinate
    }
    
    /// Delegate method called when the authorization status for the application changed.
    ///
    /// - Parameter manager: The location manager object.
    func locationManagerDidChangeAuthorization(_ manager: CLLocationManager) {
        if [.authorizedAlways, .authorizedWhenInUse].contains(locationManager.authorizationStatus) {
            locationManager.startUpdatingLocation()
        }
    }
}
