//
//  ToiletListMiddleware.swift
//  Ratp
//
//  Created by Stephane Mouawad on 15/03/2024.
//

import UIKit

/// A middleware class responsible for managing the collection view data source and layout for toilet list display.
class ToiletListMiddleware: NSObject, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    /// The reuse identifier for the collection view cells.
    let reuseIdentifier = "ToiletCardCell"
    /// The data source containing the toilet information.
    var dataSource: [Toilet] = []
    /// The location middleware instance for computing distances.
    var middleware: LocationMiddleware
    
    /// Initializes a `ToiletListMiddleware` instance with a given `LocationMiddleware`.
    ///
    /// - Parameter middleware: The `LocationMiddleware` instance to be used for computing distances.
    init(middleware: LocationMiddleware) {
        self.middleware = middleware
        super.init()
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return dataSource.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as? ToiletCardCell
        
        let toilet = dataSource[indexPath.row]
        let toiletCardCellViewModel = ToiletCardCellViewModel(toilet: toilet, middleware: middleware)
        cell?.update(withViewModel: toiletCardCellViewModel)
        return cell ?? UICollectionViewCell()
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.bounds.width * 0.80, height: collectionView.bounds.height)
    }
}
