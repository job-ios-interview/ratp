//
//  ReusableAnnotationMiddleware.swift
//  Ratp
//
//  Created by Stephane Mouawad on 14/03/2024.
//

import Foundation
import MapKit

/**
 A middleware class responsible for managing the annotations displayed on a map view.
 This class conforms to the `MKMapViewDelegate` protocol to customize the appearance of annotations.
 */
class ReusableAnnotationMiddleware: NSObject, MKMapViewDelegate {
    // MARK: - MKMapViewDelegate

    /**
     Returns a reusable annotation view or creates a new one if not available.
     
     - Parameters:
     - mapView: The map view object requesting the view.
     - annotation: The annotation object that is about to be displayed.
     
     - Returns: A reusable annotation view or `nil` if the annotation is not a `MKPointAnnotation`.
     */
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        guard annotation is MKPointAnnotation else { return nil }

        let identifier = "Annotation"
        var annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: identifier)

        if annotationView == nil {
            annotationView = MKMarkerAnnotationView(annotation: annotation, reuseIdentifier: identifier)
            annotationView?.canShowCallout = true
        } else {
            annotationView?.annotation = annotation
        }

        return annotationView
    }
}
