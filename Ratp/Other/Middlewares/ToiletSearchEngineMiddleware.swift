//
//  ToiletSearchEngineMiddleware.swift
//  Ratp
//
//  Created by Stephane Mouawad on 14/03/2024.
//

import Foundation

/// Middleware for filtering toilets based on search criteria.
struct ToiletSearchEngineMiddleware {
    /// Filters toilets based on the provided search text.
    ///
    /// - Parameters:
    ///   - entryText: The search text entered by the user.
    ///   - toilets: The array of toilets to filter.
    /// - Returns: An array of toilets that match the search criteria.
    func filter(entryText: String, toilets: [Toilet]) -> [Toilet] {
        let entryText = entryText.lowercased()
        var output: [Toilet] = []

        var reducedMobilityAccess: [Toilet] = []
        var babyChangingFacilities: [Toilet] = []

        if entryText.isEmpty {
            return toilets
        }

        toilets.forEach { toilet in
            if toilet.fullStreetAddress.lowercased().contains(entryText) {
                output.append(toilet)
            }

            if toilet.babyChangingFacilities {
                babyChangingFacilities.append(toilet)
            }

            if toilet.reducedMobilityAccess {
                reducedMobilityAccess.append(toilet)
            }
        }

        if ["prm", "pmr", "rma", "personnes à", "personnes à mobilité réduite", "reduced mobility acces"].contains(entryText) {
            output.append(contentsOf: reducedMobilityAccess)
        }

        if ["bebe", "bébé", "baby", "child"].contains(entryText) {
            output.append(contentsOf: babyChangingFacilities)
        }
        
        return output
    }
}
