//
//  ViewController.swift
//  Test
//
//  Created by Stephane Mouawad on 13/03/2024.
//

import UIKit
import Combine
import MapKit

@MainActor
class HomePageViewModel: ObservableObject {
    // MARK: - Properties
    // Layout
    let searchBarHeight: CGFloat = 45
    let collectionViewHeight: CGFloat = 250
    lazy var searchBarCornerRadious = searchBarHeight / 2
    
    // Middleware
    let reusableAnnotationMiddleware: ReusableAnnotationMiddleware
    let toiletSearchEngineMiddleware: ToiletSearchEngineMiddleware
    let toiletListMiddleware: ToiletListMiddleware
    let locationMiddleware: LocationMiddleware
    
    var searchBarViewViewModel: SearchBarViewViewModel
    private var toilets: [Toilet] = []
    
    // Binding
    @Published var dataSource: RootObject?
    @Published var presentedToilets: [Toilet] = []
    
    // Network
    let requestor = Requestor(cache: .init(validity: 60 * 15))
    
    let defaultArea: MKCoordinateRegion = {
        let coordinates = CLLocationCoordinate2D(latitude: 48.8566, longitude: 2.3522)
        
        return MKCoordinateRegion(center: coordinates,
                                  latitudinalMeters: 10000,
                                  longitudinalMeters: 10000)
    }()
    
    // MARK: - Lifecycle
    init(searchBarViewViewModel: SearchBarViewViewModel = .init(),
         reusableAnnotationMiddleware: ReusableAnnotationMiddleware = .init(),
         toiletSearchEngineMiddleware: ToiletSearchEngineMiddleware = .init(),
         toiletListMiddleware: ToiletListMiddleware,
         locationMiddleware: LocationMiddleware = .init(locationManager: .init())) {
        
        self.searchBarViewViewModel = searchBarViewViewModel
        self.reusableAnnotationMiddleware = reusableAnnotationMiddleware
        self.toiletSearchEngineMiddleware = toiletSearchEngineMiddleware
        self.toiletListMiddleware = toiletListMiddleware
        self.locationMiddleware = locationMiddleware
        
        searchBarViewViewModel.onChange = { [weak self] text in
            guard let self else { return }
            let filteredData = toiletSearchEngineMiddleware.filter(entryText: text, toilets: toilets)
            toiletListMiddleware.dataSource = filteredData
            presentedToilets = filteredData
        }
    }
    
    // MARK: - Public
    func viewDidLoad() {
        Task {
            do {
                let rootRequest = RequestBuilder.request(for: EndPoint.root)
                let dataSource: RootObject = try await requestor.fire(with: rootRequest)
                
                toilets = dataSource.records.compactMap { record in
                    Toilet(fields: record.fields)
                }
                
                presentedToilets = toilets
                toiletListMiddleware.dataSource = presentedToilets
                
                self.dataSource = dataSource
                
            } catch {
                print(error)
            }
        }
    }
    
    func viewDidAppear() {
        locationMiddleware.startUpdatingLocation()
    }
    
    func viewDidDisappear() {
        locationMiddleware.stopUpdatingLocation()
    }
}

// MARK: - ViewController
class ViewController: UIViewController {
    // MARK: - Properties
    private let searchBarView: SearchBarView
    private let mapView = MKMapView()
    var viewModel: HomePageViewModel
    var anyCancellable: AnyCancellable?
    
    private let collectionView: UICollectionView = {
        let flowLayout = UICollectionViewFlowLayout()
        flowLayout.sectionInset = UIEdgeInsets(top:0, left: 20, bottom: 0, right: 0)
        flowLayout.scrollDirection = .horizontal
        
        let collection = UICollectionView(frame: .zero, collectionViewLayout: flowLayout)
        collection.backgroundColor = .clear
        return collection
    }()
    
    // MARK: - Lifecycle
    init(viewModel: HomePageViewModel) {
        self.viewModel = viewModel
        self.searchBarView = SearchBarView(viewModel: viewModel.searchBarViewViewModel)
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        let locationMiddleware = LocationMiddleware(locationManager: .init())
        self.viewModel = HomePageViewModel(toiletListMiddleware: .init(middleware: locationMiddleware))
        self.searchBarView = SearchBarView(viewModel: viewModel.searchBarViewViewModel)
        super.init(coder: coder)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupFront()
        setupLayout()
        viewModel.viewDidLoad()
        
        anyCancellable = viewModel
            .objectWillChange
            .receive(on: DispatchQueue.main)
            .sink { [weak self] in
                guard let self else { return }
                
                collectionView.reloadData()
                mapView.removeAnnotations(mapView.annotations)
                
                guard viewModel.presentedToilets.isEmpty == false else { return }
                
                viewModel.presentedToilets.forEach {
                    let annotation = MKPointAnnotation()
                    annotation.coordinate = $0.location
                    self.mapView.addAnnotation(annotation)
                }
                
                
                mapView.annotations.first.map {
                    self.mapView.showAnnotations([$0], animated: true)
                }
            }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        viewModel.viewDidAppear()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        viewModel.viewDidDisappear()
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
        view.endEditing(true)
    }
    
    // MARK: - Privates
    private func setupFront() {
        mapView.delegate = viewModel.reusableAnnotationMiddleware
        mapView.showsCompass = false
        mapView.setRegion(viewModel.defaultArea, animated: false)
        
        collectionView.register(ToiletCardCell.self, forCellWithReuseIdentifier: viewModel.toiletListMiddleware.reuseIdentifier)
        collectionView.delegate = viewModel.toiletListMiddleware
        collectionView.dataSource = viewModel.toiletListMiddleware
        searchBarView.layer.cornerRadius = viewModel.searchBarCornerRadious
        
        [mapView, searchBarView, collectionView].forEach {
            view.addSubview($0)
        }
    }
    
    private func setupLayout() {
        [searchBarView, mapView, collectionView].forEach {
            $0.translatesAutoresizingMaskIntoConstraints = false
        }
        
        NSLayoutConstraint.activate([
            // Setup constraints for mapView
            mapView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            mapView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            mapView.topAnchor.constraint(equalTo: view.topAnchor),
            mapView.bottomAnchor.constraint(equalTo: view.bottomAnchor),
            
            // Setup constraints for searchBarView
            searchBarView.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor, constant: 10),
            searchBarView.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor, constant: -10),
            searchBarView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 10),
            searchBarView.heightAnchor.constraint(equalToConstant: viewModel.searchBarHeight),
            
            // Setup constraints for searchBarView
            collectionView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            collectionView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            collectionView.bottomAnchor.constraint(equalTo: view.keyboardLayoutGuide.topAnchor),
            collectionView.heightAnchor.constraint(equalToConstant: viewModel.collectionViewHeight),
        ])
    }
}
